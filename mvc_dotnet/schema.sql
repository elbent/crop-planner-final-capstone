﻿
-- Switch to the system (aka master) database
USE master;
GO

-- Delete the CropPlannerDB Database (IF EXISTS)
IF EXISTS(select * from sys.databases where name='CropPlannerDB')
DROP DATABASE CropPlannerDB;
GO

-- Create a new CropPlannerDB Database
CREATE DATABASE CropPlannerDB;
GO

-- Switch to the CropPlannerDB Database
USE CropPlannerDB
GO

BEGIN TRANSACTION;

CREATE TABLE users
(
	id			int			identity(1,1),
	username	varchar(50)	not null,
	password	varchar(50)	not null,
	salt		varchar(50)	not null,
	role		varchar(50)	default('user'),

	constraint pk_users_id primary key (id)
);

Create Table plot
(
	id				int				identity(1,1),
	name			varchar(50) not null,
	length			decimal		not null,
	width			decimal		not null,
	exposureToSun	varchar(50) not null,
	userId			int		not null,

	constraint pk_plot_id primary key (id),
	CONSTRAINT fk_plot FOREIGN KEY (userId) REFERENCES users(id)
);

Create Table plant
(
	id				int				identity(1,1),
	name			varchar(50) not null,
	description		varchar(1000) not null,
	exposureToSun	varchar(50) not null,
	spread			int		not null,
	typeSold		varchar(50) not null,
	quantity		int  not null,
	price			decimal not null,
	img				varchar(50) not null,
	color			varchar(50) not null

	constraint pk_plant_id primary key (id)
);

Create Table soil
(
	id				int				identity(1,1),
	name			varchar(50) not null,
	price			decimal not null,
	coverage		decimal not null,
	

	constraint pk_soil_id primary key (id)
);


Create Table plantPlot
(
	id			int identity (1,1),
	plotId		int not null,
	plantId		int not null,
	locationX   int not null,
	locationY   int not null,

	constraint pk_plantPlot_plotId_plantId Primary Key (id),
	CONSTRAINT fk_PlantPlot_Plot FOREIGN KEY (plotId) REFERENCES plot(id),
	CONSTRAINT fk_PlantPlot_Plant FOREIGN KEY (plantId) REFERENCES plant(id)
);

Create Table soilPlot
(
	plotId		int not null,
	soilId		int not null,
	quantity	int not null, 

	constraint pk_plantPlot_plotId_soilId Primary Key (plotId, soilId),
	CONSTRAINT fk_SoilPlot_Plot FOREIGN KEY (plotId) REFERENCES plot(id),
	CONSTRAINT fk_SoilPlot_Soil FOREIGN KEY (soilId) REFERENCES soil(id)
);

Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Roma Tomato', 'Compact plants produce paste-type tomatoes resistant to Verticillium and Fusarium wilts. Meaty interiors and few seeds. GARDEN HINTS: Fertilize when first fruits form to increase yield. Water deeply once a week during very dry weather.', 'Full Sun', '18', 'Plant', '3', '17.99', 'romatomato.jpg', 'blue');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Cherry Roma Tomato', 'Plum-cherry fruits bursting with sweet flavor. Produces an abundance of 2\" plum-cherry fruits bursting with sweet fresh grape tomato flavor. Ready to add sparkle to a salad or become an instant snack.', 'Full Sun', '18', 'Plant', '3', '17.99', 'cherryroma.jpg', 'blueviolet');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Italian Ribbed Zucchini', 'Italian heirloom with nutty taste and star-shaped slices. Known and beloved by Italians as "Costata Romanesco", this Italian heirloom offers up a distinctive nutty flavor. The 6-8\" medium-green fruits have pale green ribs and flecks. The prominent ribs create star-shaped slices which are lovely sauteed or roasted.', 'Full Sun', '60','Seeds', '40' , '4.19', 'ribbedzucchini.jpg', 'green');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Black Beauty Squash', 'Summer type. Great to eat any way you cook it. Glossy black-green zucchini with creamy, white flesh. Plants have an open habit which makes for easy picking. Best picked when 6 to 8\" long.', 'Full Sun', '90', 'Seeds', '100', '4.99', 'blackbeautysquash.jpg', 'burlywood');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Golden Bantam Corn', 'HEIRLOOM. This variety made yellow sweet corn popular. Orig. 1902.  This variety made yellow sweet corn popular. When Burpee introduced it in 1902, people only wanted white corn, as white signified refinement and quality. It was created by a skilled gardener in Greenfield, Massachusetts who loved to have the earliest corn in town. Golden Bantam quickly rose to the top since it sprouted in cool soil better than all other corns of the time, and growers could make big money with it. The stalks are only 5 ft. tall and often bear two 5 1/2 to 6 1/2\" long ears apiece. For old- fashioned corn flavor and early plantings it is, still outstanding.', 'Full Sun', '12','Seeds', '200', '4.99', 'goldenbantamcorn.jpg', 'cadetblue');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Spearmint Mint', 'Leaves impart flavor to iced drinks, sauces, vegetables and lamb.  Most of the mints we use today, including spearmint, came to North America with the Colonists. They used mint teas medicinally for headaches, indigestion and to help them sleep. Mint is also an excellent culinary addition and makes a great teas for the pure pleasure of it. As a general rule, mint family plants root vigorously when allowed to grow freely and can be invasive. Grow them in containers to keep them in check.', 'Full Sun, Partial Sun', '12', 'Seeds', '200', '3.19', 'spearmint.jpg', 'coral');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Pacific Purple Asparagus', 'Florid purple color and love-me-tenderness.  We are passionate about this asparagus florid purple color and love-me-tenderness. The spears surprising and delectable mild flavor and color merit a starring role in hors d\''oeuvres and salads. Grows up to 5\'' tall. ', 'Full Sun, Partial Sun', '24', 'Roots', '25', '36.99', 'pacificpurpleasparagus.jpg', 'chocolate');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Heritage Raspberry', 'Everbearing raspberry for fall bearing with great color, flavor, firmness and freezing quality.  Luscious flavor and heavy yields of juicy, sweet, red fruits. Plants produce berries on old canes in early summer and on new canes from August to frost. Vigorous and hardy. Self-pollinating. Grows best in full sun.', 'Full Sun', '36', 'Plant', '1', '15.79', 'heritagerasberry.jpg', 'darkred');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Crimson Sweet Watermelon', 'Large 25 lb. striped watermelon. Large, round melons averaging 25 lb. are light green with dark green stripes. Flesh is dark red, firm and fine-textured. Resistant to Fusarium wilt and anthracnose. GARDEN HINTS: For early fruiting and to overcome a short growing season, start seeds in a warm, well-lighted indoor area 3 to 4 weeks before last spring frost. Before transfer to garden, accustom plants to outdoor conditions by moving to a sheltered area outside for a week. Grow on plastic mulch to control weeds, conserve soil moisture and protect fruit by keeping it off the ground.', 'Full Sun', '72', 'Seeds', '100', '5.19', 'crimsonsweetwatermelon.jpg', 'red');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Connecticut Field Pumpkin', 'HEIRLOOM. It is the original Halloween pumpkin and makes great pies too.  When the first settlers arrived in New England, native Americans were growing pumpkins that looked like this one among their corn. It is the original Halloween pumpkin and makes great pies too. Fruits measure 12-20" in diameter and weigh 15-25 lbs.', 'Full Sun', '72', 'Seeds', '50', '4.19', 'connecticutfieldpumpkin.jpg', 'darkorange');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Japanese Painted Fern', 'Beautiful fronds with wine mid-ribs and silver-green and white leaflets.  Athyrium niponicum pictum is the deciduous Japanese Painted fern. Its beautiful 8-12" tall fronds have wine colored mid-ribs and "painted" silver-green and white leaflets.', 'Full Shade, Partial Sun', '18', 'Plant', '1', '12.59', 'japanesepaintedfern.jpg', 'mediumblue');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Hydrangea Moonrock', 'Large white flower clusters with lime green centers are a modern masterpiece.  Serenely elegant shrub produces a flurry of moonscape-textured creamy white blooms with cool lime green centers. As summer transitions to fall, flowers take on enticing apple-green hues. Easy to grow midsize 4–6\'' plants are marvels in the landscape or anchoring a mixed border.', 'Full Shade, Partial Sun', '60', 'Plant', '1', '19.99', 'hydrangeamoonrock.jpg', 'yellowgreen');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Hellebore Ivory Prince', 'Late winter blooms! Green-tinted ivory petals that age to rose.  Rich burgundy pink buds open in late winter to reveal green-tinted ivory petals that age to rose. The vigorous and hardy hellebore, or "Lenten rose", blooms in the winter and early spring garden before other plants awaken. Highly deer resistant, too.', 'Full Shade, Partial Sun', '26', 'Plant', '1', '19.99', 'helleboreivoryprince.jpg', 'olive');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Summer Wave Large Blue Torenia', 'Serene blue wishbone flowers are a favorite of children and pollinators alike.  Glorious blue flowers—as open and delicate as orchids—offer an invitation to hidden treasures: at the center of each bloom is a magical wishbone. Both children and hummingbirds delight as ‘Summer Wave’ cascades from hanging baskets and containers. Best planted in partial shade to give the most blooms during summer heat.', 'Partial Sun', '24', 'Plant', '4', '19.99', 'summerwavetorenia.jpg', 'olivedrab');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Olive Mosaic Coleus', 'Enchanting, multicolored leaves create a tropical spectacle in the shade.  Coleus is a foliage plant par excellence, a favorite of discerning gardeners since Victorian times. ‘Olive Mosaic’s enchanting, multicolored oblate leaves, with their hand-painted look, create a tropical spectacle in the shade from spring until late summer. Produce low-key show of blue-to white nettlelike flowers in late summer. Well-branched, vigorous, upright 18-26” tall plants will tolerate sun in hot, humid summers. Shade and sun.', 'Full Sun, Full Shade', '22', 'Seeds', '20', '6.29', 'olivemosaiccoleus.jpg', 'orchid');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Lime Basil', 'Sweet and fragrant basil with a mild citrus taste and fragrance. A rare and hard to find seed variety, Lime basil is sweet and fragrant with a mild citrus taste. The lime scent of the bright green, lance-shaped leaves pair perfectly with lemon basil for a full explosion of tangy fresh flavor. Excellent for flavoring sauces, dressings and desserts.', 'Full Sun', '10', 'Seeds', '600', '4.19', 'limebasil.jpg', 'indigo');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Hot & Spicy Oregano', 'Green foliage is spicier than Italian Oregano. Used in salsa and chili dishes. As the name suggest, this pungent oregano has the bold flavors that are the building blocks of Mexican dishes. Also a good substitute for recipes calling for common oregano, though reduce amount by half of what the recipe calls for.', 'Full Sun', '12', 'Plants', '3', '17.99', 'hotspicyoregano.jpg', 'lightpink');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Herkules Dill', '\''Super-charged\'' herb with extra large leaves and flowers. Herkules is a tetraploid, meaning the leaves and flowers are extra large. Plus, it has been bred to increase the content of the active flavor ingredient. The result is a \''super-charged\'' herb and aromatic addition to grilled salmon, fall soups and, of course, pickled cucumbers. Provide full sun and rich, well-drained soil for best results.', 'Full Sun, Full Shade', '18', 'Seeds', '400', '3.99', 'herkulesdill.jpg', 'teal');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Rosemary', 'Fragrant leaves flavor meats, poultry and potatoes. Rosemary is an essential ingredient for holiday turkeys, but it adds wonderful flavor to meats, other poultry and vegetables too. It also makes a good addition to potpourris. Start seed early indoors. Plants tolerate light frost; set outside early.', 'Full Sun', '24', 'Seeds', '200', '5.19', 'rosemary.jpg', 'lime');
Insert Into plant (name, description, exposureToSun, spread, typeSold, quantity, price, img, color) Values ('Thyme', 'Aromatic leaves season meats, poultry, stews, sauces, soups and dressings. Thyme is one of the most widely used culinary herbs. It is commonly grown as a decorative and functional plant in many home gardens, and bees use its pollen to make delectable honey. It is easy to grow and adaptable to most soils.', 'Full Sun', '8', 'Seeds', '1800', '4.19', 'thyme.jpg', 'mediumpurple');


COMMIT TRANSACTION;